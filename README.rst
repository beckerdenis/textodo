TexTodo
=======

About
-----

TexTodo is a Python library/executable to help writing LaTeX documents.

You write a big LaTeX document with a bibliography (random example: a PhD
thesis) and you want to monitor what needs to be done, the progress,
the use of the bibliography, link between ideas, etc.

This tool uses two commands `\todo` and `\done` that can be overriden
in the LaTeX source files to do something in the resulting PDF. Otherwise,
they can be commented out for LaTeX and still be taken into account by
TexTodo. 

It is possible to use TexTodo without putting any `\todo` or `\done`
commands in a LaTeX files, because some features does not use them.
A complete help with examples is accessible with:

    textodo --help


Install dependencies
--------------------

To install the dependencies, run the following command:

    pip3 install -r requirements.txt [--user]

The '--user' options goes for a user install (in ~/.local).


Install TexTodo
---------------

To install TexTodo, run the following command:

    python3 setup.py install [--user]

The command 'textodo' will be available (embedded help) as well as
the 'textodo' python package.


Examples
--------

List Todo Items
~~~~~~~~~~~~~~~

To list the todo/done items from LaTeX files, simply run:

    textodo

Monitor Progression
~~~~~~~~~~~~~~~~~~~

To monitor the progression of writing, run periodically the command:

    textodo --record record.txt bibliography.bib

This will store information about all the `tex` files in the current
directory and the provided bibliography file into the file `record.txt`.
To plot the overall progression curves, run:

    textodo --plot record.txt

Show Todo Graph
~~~~~~~~~~~~~~~

To show a graph representing todo items and the links between them,
use the following command:

    textodo --graph

