#!/usr/bin/env python3

"""Todo script for writing tex files."""

import setuptools

import textodo

setuptools.setup(
    name='textodo',
    version=textodo.__version__,
    packages=setuptools.find_packages(),
    author='Denis Becker',
    description='Todo script for writing tex files',
    long_description=open('README.rst').read(),
    install_requires=[
        'graphviz>=0.5.2',
        'matplotlib>=2.0.0rc2',
        'bibtexparser>=0.6.2'
    ],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'textodo = textodo.textodo:main',
        ]
    }
)
