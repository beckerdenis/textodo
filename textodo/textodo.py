#!/usr/bin/env python3

"""
Tool for LaTeX: put 'todo' in a LaTeX file, and this
tool prints information/progression about it.
"""
import argparse
import glob
import re
import enum
import os
import sys
import tempfile
import textwrap
import datetime
import graphviz
import matplotlib.pyplot
import bibtexparser

__version__ = '0.6'

HELP_DESCRIPTION_TEXT = '''Show information about TODO items in LaTeX files.
The default behavior shows both TODO items and done items.'''

HELP_EPILOG_TEXT = '''Some examples:
  textodo                           # Display todo and done items for all tex files
  textodo --graph                   # Display dependency graph for all tex files
  textodo --graph file.svg main.tex # Save the graph for "main.tex" in "file.svg"
  textodo --biblio my.bib main.tex  # Find bib entries in "my.bib" not cited in "main.tex"

Format of a TODO item in a LaTeX file: \\todo{key}{key,...}{description}
TODO items can be commented out (LaTeX style). Not commenting items enable to
define a LaTeX command in the document to do something with these items
(e.g. generating lorem ipsum).

The first parameter is the key of this todo (no spaces).
The list of keys after that is comma-separated and gives the dependencies
of this item (can be left empty).
The last parameter is a short description of what should be written here.

In a line defining a TODO item, the TODO item must be the first thing of the
line (except separators). It may be spread across multiple lines, but those lines
should not contain something else (except comment marks).

Examples:
  \\todo{a}{b, c}{Some item.}                % TODO item
  Some text.\\todo{a}{b, c}{Some item.}      % not TODO item
  \\newcommand\\todo...                       % not TODO item
  %\\todo{c}{}{Item.}                        % TODO item

To mark a TODO item as done, replace \\todo with \\done:

Examples:
  \\todo{a}{b, c}{Some item.}                % TODO item
  \\done{d}{b}{Some item again.}             % item done
  %\\todo{b}{c}{Some other item.}            % TODO item
  %\\done{c}{}{Some different item.}         % item done
'''

# Some console color definitions

ERROR_COLOR = '\033[91m'
WARNING_COLOR = '\033[93m'
DONE_COLOR = '\033[94m'
CLEAR_COLOR = '\033[0m'

# Constants

ORDERED_LEVELS = ['document', 'part', 'chapter', 'section', 'subsection']
LEVEL_COLORS = [None, 'red', 'blue', 'green', 'purple']

# Enums

class ItemState(enum.Enum):
    """Represent the state of an item."""
    TODO = 'todo'
    DONE = 'done'

    def __str__(self):
        """Prints a checkbox with a color depending on the state."""
        if self == ItemState.TODO:
            return u'\u2610'
        else:
            return cond_color(u'{}\u2612{}', DONE_COLOR)

# Model

class Document():
    """Represent the document."""

    def __init__(self):
        """Constructor."""
        self.document = TodoItemGroup('document', ORDERED_LEVELS[0])
        self.all_items = []

    def add_item(self, file, item_type):
        """Add an item to the innermost group.

        Args:
            file        File at right position.
            item_type   Type of the item.
        """
        item = build_item(file, item_type)
        self.document.add_item(item)
        self.all_items.append(item)

    def add_group(self, name, level):
        """Change the group at the given level with a new group.

        Args:
            name    Name of the new group.
            level   Type of the new group (e.g. section, chapter).
        """
        self.document.add_group(name, level)

    def build_graph(self, noedge, output_file=None):
        """Build graph for this document.

        Args:
            noedge          If True, edges will not be rendered.
            output_file     Name of the output file. If none, a
                            temporary file is created.
        """
        if output_file is None:
            output_file = os.path.join(tempfile.gettempdir(), 'todo_latex_view_graph.pdf')
            warning('Generated temporary file {!r}'.format(output_file))

        dirname, filename = os.path.split(output_file)
        filename, formatext = os.path.splitext(filename)
        dot = graphviz.Digraph(filename=filename, directory=dirname,
                               format=formatext[1:], strict=True)

        # If noedge is specified, we still draw the edges but with 'invisible' style
        # This is to ensure that the graph will be good-looking (if there is no edge,
        # the generated graph is always one-lined, which is not good for big graphs)
        if noedge:
            dot.attr('edge', style='invis')

        # Nodes and dependencies
        for item in self.all_items:
            # We must use escaped new lines, otherwise the generated dot source is wrong
            description = '\\n'.join(textwrap.wrap(item['description'], 28))
            if item['state'] == ItemState.TODO:
                dot.node(item['key'], label=description)
            else:
                dot.node(item['key'], label=description, style='filled')
            for dependency in item['dependencies']:
                dot.edge(dependency['key'], item['key'])

        # Add groups (e.g. sections, chapters) as subgraph clusters
        for group in self.document.groups:
            dot.subgraph(group.get_subgraph([]))

        return dot

    def check_constraints(self):
        """Check the following constraints:

        Integrity: each dependency must be an existing key.
        Unicity: each dependency must be distinct from any other.
        """
        error_list = []
        key_list = [item['key'] for item in self.all_items]

        # Integrity
        for item in self.all_items:
            for dependency in item['dependencies']:
                if dependency['key'] not in key_list:
                    error_list.append('Dependency {!r} (from item {!r}) has no matching todo item.'
                                      .format(dependency['origkey'], item['origkey']))

        # Unicity
        okey_list = [item['origkey'] for item in self.all_items]
        while okey_list:
            okey = okey_list.pop()
            if okey in okey_list:
                error_list.insert(0, 'Duplicate key: {!r}.'.format(okey))

        # Print errors
        if error_list:
            while error_list:
                error(error_list.pop(), exit_after=False)
            exit()

class TodoItemGroup():
    """Represent a group of TODO items."""

    def __init__(self, name, level):
        """Constructor.

        Args:
            name    Name of the group.
            level   Type of group (e.g. section, chapter).
        """
        self.key = format_key(name)
        self.name = name
        self.level = level
        self.groups = []
        self.items = []

    def __str__(self):
        """Print the item group in the console."""
        index_self = ORDERED_LEVELS.index(self.level)
        text = self.level + ':{\n' + '    ' * (index_self + 1)
        text += self.name + '\n' + '    ' * (index_self + 1)
        for group in self.groups:
            text += str(group)
        for item in self.items:
            text += 'ITEM:' + item['origkey'] + '\n' + '    ' * (index_self + 1)
        text = text[:-4]
        text += '}\n' + '    ' * index_self
        return text

    def add_item(self, item):
        """Add an item to the innermost group.

        Args:
            item        Item to add.
        """
        if self.groups:
            self.groups[-1].add_item(item)
        else:
            self.items.append(item)

    def add_group(self, name, level):
        """Change the group at the given level with a new group.
        Predicate: level (of the new group) is below self.level.
        Returns True if the group was added. Call this method
        from a document should always return True.

        Args:
            name    Name of the new group.
            level   Type of the new group (e.g. section, chapter).
        """
        level_candidate = len(ORDERED_LEVELS) - ORDERED_LEVELS.index(level)
        level_self = len(ORDERED_LEVELS) - ORDERED_LEVELS.index(self.level)
        # Defensive: predicate
        if level_candidate >= level_self:
            error('Internal error with document hierarchy.')
        # No subgroups yet: simply add the candidate
        if not self.groups:
            self.groups.append(TodoItemGroup(name, level))
            return True
        # Subgroups present: recursive check
        level_subgroup = len(ORDERED_LEVELS) - ORDERED_LEVELS.index(self.groups[-1].level)
        if level_candidate == level_subgroup:
            self.groups.append(TodoItemGroup(name, level))
            return True
        elif level_candidate < level_subgroup:
            if not self.groups[-1].add_group(name, level):
                self.groups.append(TodoItemGroup(name, level))
            return True
        return False

    def get_subgraph(self, title_stack):
        """Return the subgraph for this group."""
        title_stack = list(title_stack) # clone list
        title_stack.append(self.key)

        graph = graphviz.Digraph(name='cluster_{}'.format('_'.join(title_stack)), graph_attr={
            'color': LEVEL_COLORS[ORDERED_LEVELS.index(self.level)],
            'label': self.name
        })

        # First get the subgroups' graphs
        for group in self.groups:
            graph.subgraph(group.get_subgraph(title_stack))

        # Then append self items
        for item in self.items:
            graph.node(item['key'])

        return graph

# Reporting

def error(message, suffix=None, exit_after=True):
    """Report an error and exit."""
    print(cond_color('{}Error:{} ' + message, ERROR_COLOR))
    if suffix is not None:
        print(suffix)
    if exit_after:
        exit()

def warning(message, suffix=None):
    """Report a warning."""
    print(cond_color('{}Warning:{} ' + message, WARNING_COLOR))
    if suffix is not None:
        print(suffix)

def cond_color(formatted_message, color_if_enabled):
    """ Returns the formatted message with colors (if output is a tty).
    Example: cond_color('{}Error{}: something is wrong', '\\033[94m')
    Returns: 'Error: something is wrong' if output is not a terminal
             '\\033[94mError\\033[0m: something is wrong' otherwise
    """
    if sys.stdout.isatty():
        return formatted_message.format(color_if_enabled, CLEAR_COLOR)
    else:
        return formatted_message.format('', '')

# Utilities

def format_key(key):
    """Format the key to contain only alphanumeric and underscore, removing %."""
    key = key.strip()
    for ritem in [' ', '\\', '+', '-', ':', '(', ')', '\n']:
        key = key.replace(ritem, '_')
    return key.strip().replace('%', '')

def read_until(file, stopchar):
    """Read from file until stopchar is met (excluded)."""
    acc = ''
    char = file.read(1)
    while char != stopchar:
        acc += char
        char = file.read(1)
    return acc

def read_until_and_ignore_commands(file, stopchar):
    """Read from file until stopchar is met (excluded).
    Excludes latex commands (from backslash to separator).
    """
    if stopchar == '\\':
        error('Implementation error, stopchar \\ not allowed.')

    acc = ''
    while True:
        char = file.read(1)
        if char == '\\':
            while char != ' ' and char != '}' and char != '\n':
                char = file.read(1)
        elif char == stopchar:
            break
        else:
            acc += char
    return acc

def grep(filenames, expression):
    """Return true only if expression is in one of the files."""
    for filename in filenames:
        with open(filename, 'r') as file:
            for line in file:
                if re.search(expression, line):
                    return True
    return False

def build_item(file, item_type):
    """Return a todo item that is read from current position in file."""
    item = {}
    item['origkey'] = read_until(file, '}').strip()
    item['key'] = format_key(item['origkey'])
    read_until(file, '{')
    dependencies = read_until(file, '}').replace('\n', ' ')
    item['dependencies'] = ([] if not dependencies.strip() else
                            [{
                                'origkey': dep.strip(),
                                'key': format_key(dep)
                            } for dep in dependencies.split(',')])
    read_until(file, '{')
    item['description'] = read_until(file, '}').replace('\n%', ' ').replace('\n', ' ').strip()
    item['state'] = ItemState(item_type)
    return item

def scan_references(file, reference_list):
    """Scans references from current position of the file.
    References are a list of comma-separated items.
    """
    refs = read_until(file, '}')
    for ref in refs.split(','):
        reference_list.append(ref.strip())

def build_label_and_reference_list(filenames):
    """Build the label and reference lists."""
    label_list = []
    reference_list = []
    for filename in filenames:
        with open(filename, 'r') as file:
            while True:
                char = file.read(1)
                if not char:
                    break
                if char == '\\':
                    command = read_until(file, '{')
                    if command in ['ref', 'cref', 'Cref']:
                        scan_references(file, reference_list)
                    elif command == 'label':
                        label_list.append(read_until(file, '}'))
    return label_list, reference_list

def build_document(filenames):
    """Build the item list from all files."""
    beginline = True
    document = Document()
    for filename in filenames:
        with open(filename, 'r') as file:
            while True:
                char = file.read(1)
                if not char:
                    break
                if char == '\n':
                    beginline = True
                elif char == '\\' and beginline:
                    command = read_until(file, '{')
                    if command in ['todo', 'done']:
                        document.add_item(file, command)
                    elif command in ORDERED_LEVELS:
                        document.add_group(read_until_and_ignore_commands(file, '}')
                                           .replace('\n', ' '), command)
                elif char.strip() not in ['', '%']:
                    beginline = False
    document.check_constraints()
    return document

def build_bibitem_list(filenames, biblio):
    """Build a bib item list from a bibliography file, corresponding to all the files."""
    item_list = []
    try:
        with open(biblio, 'r') as file:
            for line in file:
                match = re.search('@.*{(.*),', line)
                if match:
                    item_list.append({
                        'key': match.group(1)
                    })
    except FileNotFoundError:
        error('File not found: {!r}'.format(biblio))
    for item in item_list:
        item['state'] = ItemState.DONE if grep(filenames, item['key']) else ItemState.TODO
    return item_list

def build_author_list(bib_database):
    """Build the author list for each entry of a database."""
    for entry in bib_database.entries:
        if 'author' in entry:
            entry['authors'] = entry['author'].split(' and ')
            for idx, auth in enumerate(entry['authors']):
                entry['authors'][idx] = auth.split(',')[0].split(' ')[-1]

# Commands

def show_statistics(filenames):
    """Show statistics for a set of latex files."""
    document = build_document(filenames)
    nb_todo = sum(1 for item in document.all_items if item['state'] == ItemState.TODO)
    nb_done = sum(1 for item in document.all_items if item['state'] == ItemState.DONE)
    print('Number of TODO:\t{:8d}'.format(nb_todo))
    print('Number of DONE:\t{:8d}'.format(nb_done))
    if nb_todo != 0 or nb_done != 0:
        print('Accomplishment:\t{:8.3g} %'.format(100 * nb_done / (nb_done + nb_todo)))

def save_statistics(filenames, biblio, stat_filename):
    """Append current statistics with date in the end of the file denoted by stat_filename."""
    document = build_document(filenames)
    nb_todo = sum(1 for item in document.all_items if item['state'] == ItemState.TODO)
    nb_done = sum(1 for item in document.all_items if item['state'] == ItemState.DONE)
    item_list = build_bibitem_list(filenames, biblio)
    nb_bibitem_todo = sum(1 for item in item_list if item['state'] == ItemState.TODO)
    nb_bibitem_done = sum(1 for item in item_list if item['state'] == ItemState.DONE)
    count_lines = sum(sum(1 for line in open(filename)) for filename in filenames)
    if not os.path.isfile(stat_filename):
        with open(stat_filename, 'w') as sfile:
            sfile.write('# Date(ISO) Nb_todo Nb_item Nb_bib_todo Nb_bib_item Line_count\n')
    with open(stat_filename, 'a') as sfile:
        sfile.write('{} {} {} {} {} {}\n'.format(datetime.date.today().isoformat(),
                                                 nb_todo,
                                                 nb_todo + nb_done,
                                                 nb_bibitem_todo,
                                                 nb_bibitem_todo + nb_bibitem_done,
                                                 count_lines))

def show_missing_references(filenames):
    """Show the list of missing references (uncited labels)."""
    label_list, reference_list = build_label_and_reference_list(filenames)
    uncited_item_found = False
    for label in label_list:
        if label not in reference_list:
            uncited_item_found = True
            print(label)
    if not uncited_item_found:
        print('No unreferenced labels found')

def show_items(filenames, state=None):
    """Show the list of todos for a set of latex files (may be filtered by state)."""
    document = build_document(filenames)
    for item in [item for item in document.all_items if state is None or state == item['state']]:
        print('\t{!s}  {}'
              .format(item['state'], textwrap.fill(item['description']).replace('\n', '\n\t   ')))

def show_biblio(filenames, biblio):
    """Show a list of uncited bibliography items for a set of latex files."""
    item_list = build_bibitem_list(filenames, biblio)
    for bibitem in item_list:
        print('\t{!s}  {}'.format(bibitem['state'], bibitem['key']))
    nb_todo = sum(1 for item in item_list if item['state'] == ItemState.TODO)
    nb_done = sum(1 for item in item_list if item['state'] == ItemState.DONE)
    print('\nCited items:\t{:8d}'.format(nb_done))
    print('Uncited items:\t{:8d}'.format(nb_todo))
    if nb_todo != 0 or nb_done != 0:
        print('Accomplishment:\t{:8.3g} %'.format(100 * nb_done / (nb_done + nb_todo)))

def show_graph(filenames, noedge):
    """Show the dependency graph for todos in a set of latex files."""
    document = build_document(filenames)
    dot = document.build_graph(noedge)
    dot.view(cleanup=True)

def save_graph(filenames, noedge, output_file):
    """Save the dependency graph for todos in a set of latex files."""
    document = build_document(filenames)
    try:
        dot = document.build_graph(noedge, output_file)
        dot.render(cleanup=True)
    except ValueError:
        error('Output format not supported.',
              textwrap.fill('Supported formats are: ' + ', '.join(sorted(graphviz.FORMATS))))

def same_bib_family(nb_req_common_authors, entry1, entry2):
    """Returns true if and only if 'entry1' and 'entry2' belong in the same
    family of papers (i.e. they have 'nb_req_common_authors' authors in common).
    """
    nb_auth = 0
    for auth1 in entry1['authors']:
        for auth2 in entry2['authors']:
            if auth1 == auth2:
                nb_auth = nb_auth + 1
    return ((nb_auth >= nb_req_common_authors)
            or (nb_auth == len(entry1['authors'])) or (nb_auth == len(entry2['authors'])))

def show_bib_families(nb_req_common_authors, bibtexfile):
    """Show the bibliography families (having certain common authors)."""
    with open(bibtexfile) as bfile:
        bibdb = bibtexparser.load(bfile)
    build_author_list(bibdb)

    families = []
    for entry in (auth_entry for auth_entry in bibdb.entries if 'author' in auth_entry):
        found = False
        for family in families:
            for item in family:
                if same_bib_family(nb_req_common_authors, entry, item):
                    family.append(entry)
                    found = True
                    break
            if found:
                break
        if not found:
            families.append([entry])

    # Print results
    print('Number of families: {}\n'.format(len(families)))
    for family in families:
        for entry in family:
            print(entry['ID'])
        print()

def plot_record(filename):
    """Plot record file."""
    plt = matplotlib.pyplot
    with open(filename) as sfile:
        all_data = [row.strip().split(' ') for row in sfile if not re.search(r'^#', row.strip())]

        dates = [datetime.datetime.strptime(row[0], '%Y-%m-%d').date() for row in all_data]

        items_ratio = [100.0 * float(row[1]) / float(row[2]) for row in all_data]
        bibitems_ratio = [100.0 * float(row[3]) / float(row[4]) for row in all_data]

        plt.figure(1)
        plt.xticks(dates, dates)
        plt.title('Progression of items done ratio through time')
        plt.xlabel('Date')
        plt.ylabel('Done ratio (%)')
        plt.plot(dates, items_ratio, c='r', label='TODO items')
        plt.plot(dates, bibitems_ratio, c='b', label='Bib items')
        plt.legend()

        nb_items = [int(row[2]) for row in all_data]
        nb_bibitems = [int(row[4]) for row in all_data]

        plt.figure(2)
        plt.xticks(dates, dates)
        plt.title('Number of items through time')
        plt.xlabel('Date')
        plt.ylabel('Number of items')
        plt.plot(dates, nb_items, c='r', label='TODO items')
        plt.plot(dates, nb_bibitems, c='b', label='Bib items')
        plt.legend()

        nb_lines = [int(row[5]) for row in all_data]

        plt.figure(3)
        plt.xticks(dates, dates)
        plt.title('Number of lines in latex files through time')
        plt.xlabel('Date')
        plt.ylabel('Number of lines')
        plt.plot(dates, nb_lines, c='r', label='Nb lines')
        plt.legend()

        plt.show()

# Arguments parsing helpers

def customize_help(message):
    """Customize help message."""
    return {
        'usage: ': 'Usage: ',
        'optional arguments': 'Optional arguments',
        'positional arguments': 'Positional arguments'
    }.get(message, message)

def get_argument_parser():
    """Create and return an argument parser."""
    parser = argparse.ArgumentParser(prog='textodo',
                                     description=HELP_DESCRIPTION_TEXT,
                                     epilog=HELP_EPILOG_TEXT,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--version', action='version',
                        version='%(prog)s version {}'.format(__version__))
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--bibfamilies', metavar=('COMMON_AUTHORS', 'BIBTEX_FILE'),
                       nargs=2, help='show bibliography families (having common authors)')
    group.add_argument('-s', dest='statistics', action='store_true',
                       help='show statistics')
    group.add_argument('--misref', dest='misref', action='store_true',
                       help='show unreferenced labels')
    group.add_argument('-t', dest='todos', action='store_true',
                       help='list only TODOs')
    group.add_argument('-d', dest='dones', action='store_true',
                       help='list only completed TODOs')
    group.add_argument('--biblio', metavar='BIBTEX_FILE',
                       help='list uncited bibliography keys from BIBTEX_FILE')
    group.add_argument('--graph', metavar='FILE', nargs='?', const=False,
                       help=('show dependency graph between TODOs or save the output\n' +
                             'to the given file (extension gives the format)'))
    group.add_argument('--record', metavar=('RECORD_FILE', 'BIBTEX_FILE'), nargs=2,
                       help=('append statistics (biblio + todo) in RECORD_FILE\n' +
                             'format: DATE NB_TODO NB_ITEM NB_BIB_TODO NB_BIB_ITEM NB_LINES'))
    group.add_argument('--plot', dest='plot_record', metavar='RECORD_FILE',
                       help='plot a record file (see --record)')
    graph_group = parser.add_argument_group('Options for --graph')
    graph_group.add_argument('--no-edge', dest='noedge', action='store_true',
                             help='disable the edges (show only TODO items)')
    parser.add_argument('latex_files', metavar='INPUT_FILES', nargs='*',
                        help='tex file(s) under consideration (default: *.tex)')
    return parser

def check_special_rules(args):
    """Check rules for arguments that are not easily done with argparse."""
    if args.noedge and args.graph is None:
        warning('Option --no-edge has no effect without --graph.')
    if args.bibfamilies is not None:
        try:
            if int(args.bibfamilies[0]) <= 0:
                error('First argument of --bibfamilies option must be a positive integer.')
        except ValueError:
            error('First argument of --bibfamilies option must be a positive integer.')
    if args.record is not None:
        if re.search(r'\.bib$', args.record[0]): # first argument = record file
            error('To avoid misunderstanding, we forbid any record file with bib extension.',
                  ('Try to switch the two arguments as you may have inverted them, or change\n' +
                   'the name of the record file so that it has an other extension than bib.'))

# Main function

# pylint: disable=too-many-branches
def main():
    """Main function."""
    argparse._ = customize_help
    args = get_argument_parser().parse_args()
    check_special_rules(args)

    # By default, consider all the tex files in current directory

    if not args.latex_files:
        args.latex_files = glob.glob('*.tex')

    # Execute the asked command

    if args.statistics:
        show_statistics(args.latex_files)
    elif args.todos:
        show_items(args.latex_files, ItemState.TODO)
    elif args.dones:
        show_items(args.latex_files, ItemState.DONE)
    elif args.misref:
        show_missing_references(args.latex_files)
    elif args.bibfamilies is not None:
        show_bib_families(int(args.bibfamilies[0]), args.bibfamilies[1])
    elif args.biblio is not None:
        show_biblio(args.latex_files, args.biblio)
    elif args.graph is not None:
        if not args.graph:
            show_graph(args.latex_files, args.noedge)
        else:
            save_graph(args.latex_files, args.noedge, args.graph)
    elif args.record is not None:
        save_statistics(args.latex_files, args.record[1], args.record[0])
    elif args.plot_record is not None:
        plot_record(args.plot_record)
    else:
        show_items(args.latex_files)

    # Check if lorem ipsum is still present

    if grep(args.latex_files, 'lipsum'):
        warning('Lorem ipsum still present in the file.')

# Main function call

if __name__ == "__main__":
    main()
